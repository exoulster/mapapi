sign = function(vendor, ...) {
  UseMethod('sign')
}

sign.amap = function(vendor, path=NULL, params) {
  secret = Sys.getenv('AMAP_SECRET')
  if (secret == '') stop('Please set environment variable AMAP_SECRET')

  sorted_params = params[sort(names(params))]
  query = paste0(crul:::make_query(sorted_params), secret)
  sig = openssl::md5(query)

  new_params = modifyList(params, list(sig=sig))
  new_params
}


sign.baidu = function(vendor, path, params) {
  secret = Sys.getenv('BAIDU_MAP_SECRET')
  if (secret=='') {
    stop('Please set environment variable BAIDU_MAP_SECRET')
  }
  params = modifyList(params, list(timestamp=as.numeric(round(Sys.time()))))
  raw = paste0(crul::url_build('', path, params), secret)
  sn = openssl::md5(URLencode(raw, reserved=TRUE, repeated=TRUE))

  new_params = modifyList(params, list(sn=sn))
  new_params
}
